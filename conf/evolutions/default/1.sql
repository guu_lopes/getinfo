# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table atividades (
  id                        bigint not null,
  data                      timestamp,
  conteudo                  varchar(255),
  disciplina                varchar(255),
  tipo                      varchar(1),
  constraint ck_atividades_tipo check (tipo in ('P','L','T')),
  constraint pk_atividades primary key (id))
;

create table eventos (
  id                        bigint not null,
  data                      timestamp,
  local                     varchar(255),
  valor                     varchar(255),
  conteudo                  varchar(255),
  tipo                      varchar(1),
  constraint ck_eventos_tipo check (tipo in ('R','D','A','P','F')),
  constraint pk_eventos primary key (id))
;

create table account (
  email                     varchar(255) not null,
  name                      varchar(255),
  password                  varchar(255),
  constraint pk_account primary key (email))
;

create sequence atividades_seq;

create sequence eventos_seq;

create sequence account_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists atividades;

drop table if exists eventos;

drop table if exists account;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists atividades_seq;

drop sequence if exists eventos_seq;

drop sequence if exists account_seq;

