package models;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;



import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.EnumMapping;
import com.avaje.ebean.annotation.EnumValue;


import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import play.data.format.*;
import play.data.validation.*;

@Entity
public class Atividades extends Model {
	
	   
	@Id
	public Long id;
	@Required
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date data;
	public String conteudo;
	public String disciplina;
	@EnumMapping(nameValuePairs="PROVA=P, TRABALHO=T, LABORATORIO=L")
	public enum Tipo {
		  PROVA,  TRABALHO ,LABORATORIO;
		 }
	@Enumerated(EnumType.STRING)
	public Tipo tipo;

	
    public static Model.Finder<Long,Atividades> find = new Model.Finder(Long.class, Atividades.class);

    

   
}

