package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.avaje.ebean.annotation.EnumMapping;

import models.Atividades.Tipo;
import play.data.format.Formats;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Eventos extends Model {
	
	@Id
	public Long id;
	@Required
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date data;
	public String local;
	public String valor;
	public String conteudo;
	@EnumMapping(nameValuePairs="PALESTRA=P, FILME=F, APRESENTACAO=A, REUNIAO=R, DEBATE=D")
	public enum Tipo2 {
		  PALESTRA,  FILME ,APRESENTACAO, REUNIAO, DEBATE;
		 }
	@Enumerated(EnumType.STRING)
	public Tipo2 tipo;
    public static Model.Finder<Long,Eventos> find = new Model.Finder(Long.class, Eventos.class);
	
}