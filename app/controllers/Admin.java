package controllers;


import java.util.List;

import models.Atividades;
import models.Eventos;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.*;


//a anotation a seguir informa que todas as actions deste controllers
//devem ser feitas apenas por usuarios autenticados
/**
 * Esta classe é um controller no qual todas as suas actions sao 
 * permitidas apenas para usuarios autenticados.
 * 
 * Isto é feito com uso da anotation abaixo
 */
@Security.Authenticated(Secured.class)
public class Admin extends Controller {
	static Form<Atividades> formAtividades = Form.form(Atividades.class);	
	static Form<Eventos> formEventos =Form.form(Eventos.class);
    //CONTROLLERS PRECEDIDOS DE LOGIN
    
    	public static Result atividades() {
    		return ok(atividades.render(formAtividades));

	}
    	
    	public static Result consulta_atividades() {
    		return ok(conatividades.render("Consulta Atividades"));

	}
    	
    	public static Result addAtividade() {
    		Form<Atividades> fAtiv = formAtividades.bindFromRequest();
    		Logger.info(fAtiv.toString());
    		//a futura atividade
    		Atividades atividade = new Atividades();
    		//transforma o formulario em uma atividade
    		atividade = fAtiv.get();
    		//salva no banco  a atividade
    		atividade.save();
    		flash("success", "Atividade salva com sucesso!");
    		return consulta_atividades();
    	}
    	
    	public static Result addEventos() {
    		Form<Eventos> fEven = formEventos.bindFromRequest();
    		Logger.info(fEven.toString());
    		//o futuro evento
    		Eventos evento = new Eventos();
    		//transforma o formulario em um evento
    		evento = fEven.get();
    		//salva no banco  o evento
    		evento.save();
    		flash("success", "Evento salvo com sucesso!");
    		return consulta_eventos();
    	}
    	
    	public static Result eventos() {
    		return ok(eventos.render(formEventos));

	}
    	
    	public static Result consulta_eventos() {
    		return ok(coneventos.render("Consulta Eventos"));
    		//	return TODO;

	}
    	// FUNCAO DELETE ATIVIDADES
    	public static Result deletaAtividades (Long id) {
    		Atividades atividadeEscolhida = Atividades.find.byId(id);
    		atividadeEscolhida.delete();
    		flash("Success", "Atividade Removida com Sucesso !!!");
    		//return redirect(routes.Admin.atividades());
    		return redirect (routes.Application.index());
    	}
    	//FIM DA FUNCAO DELETE
    	
    	// FUNCAO DELETE EVENTOS
    	public static Result deletaEventos (Long id) {
    		Eventos eventoEscolhida = Eventos.find.byId(id);
    		eventoEscolhida.delete();
    		flash("Success", "Evento Removido com Sucesso !!!");
    		return redirect (routes.Application.index());
    	}
    	//FIM DA FUNCAO DELETE
    	
    	
    	
   
   
    
}
