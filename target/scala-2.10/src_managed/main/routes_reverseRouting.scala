// @SOURCE:C:/Users/Gustavo/Downloads/template/template/conf/routes
// @HASH:5286a5368e71860e1558a443b8d2c7787bf13c96
// @DATE:Thu Nov 12 08:33:56 BRST 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:46
// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers {

// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
class ReverseAdmin {
    

// @LINE:38
def consulta_eventos(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "consulta_eventos")
}
                                                

// @LINE:42
def consulta_atividades(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "consulta_atividades")
}
                                                

// @LINE:34
def eventos(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "eventos")
}
                                                

// @LINE:43
def deletaAtividades(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "atividades/" + implicitly[PathBindable[Long]].unbind("id", id) + "/del")
}
                                                

// @LINE:29
def addAtividade(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "addAtividades")
}
                                                

// @LINE:28
def atividades(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "atividades")
}
                                                

// @LINE:35
def addEventos(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "addEventos")
}
                                                

// @LINE:39
def deletaEventos(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "eventos/" + implicitly[PathBindable[Long]].unbind("id", id) + "/del")
}
                                                
    
}
                          

// @LINE:46
class ReverseAssets {
    

// @LINE:46
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:21
def ajuda(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "ajuda")
}
                                                

// @LINE:12
def logout(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:18
def historia(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "historia")
}
                                                

// @LINE:24
// @LINE:6
def home(): Call = {
   () match {
// @LINE:6
case () if true => Call("GET", _prefix)
                                                        
// @LINE:24
case () if true => Call("GET", _prefix + { _defaultPrefix } + "home")
                                                        
   }
}
                                                

// @LINE:15
def sobre(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sobre")
}
                                                

// @LINE:11
def authenticate(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:7
def index(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "index")
}
                                                

// @LINE:10
def login(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          
}
                  


// @LINE:46
// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers.javascript {

// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
class ReverseAdmin {
    

// @LINE:38
def consulta_eventos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.consulta_eventos",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "consulta_eventos"})
      }
   """
)
                        

// @LINE:42
def consulta_atividades : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.consulta_atividades",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "consulta_atividades"})
      }
   """
)
                        

// @LINE:34
def eventos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.eventos",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "eventos"})
      }
   """
)
                        

// @LINE:43
def deletaAtividades : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deletaAtividades",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "atividades/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/del"})
      }
   """
)
                        

// @LINE:29
def addAtividade : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.addAtividade",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addAtividades"})
      }
   """
)
                        

// @LINE:28
def atividades : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.atividades",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "atividades"})
      }
   """
)
                        

// @LINE:35
def addEventos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.addEventos",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addEventos"})
      }
   """
)
                        

// @LINE:39
def deletaEventos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deletaEventos",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "eventos/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/del"})
      }
   """
)
                        
    
}
              

// @LINE:46
class ReverseAssets {
    

// @LINE:46
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:21
def ajuda : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.ajuda",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ajuda"})
      }
   """
)
                        

// @LINE:12
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:18
def historia : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.historia",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "historia"})
      }
   """
)
                        

// @LINE:24
// @LINE:6
def home : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.home",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "home"})
      }
      }
   """
)
                        

// @LINE:15
def sobre : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.sobre",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sobre"})
      }
   """
)
                        

// @LINE:11
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:7
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "index"})
      }
   """
)
                        

// @LINE:10
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:46
// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers.ref {


// @LINE:43
// @LINE:42
// @LINE:39
// @LINE:38
// @LINE:35
// @LINE:34
// @LINE:29
// @LINE:28
class ReverseAdmin {
    

// @LINE:38
def consulta_eventos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.consulta_eventos(), HandlerDef(this, "controllers.Admin", "consulta_eventos", Seq(), "GET", """ Pagina consulta de eventos""", _prefix + """consulta_eventos""")
)
                      

// @LINE:42
def consulta_atividades(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.consulta_atividades(), HandlerDef(this, "controllers.Admin", "consulta_atividades", Seq(), "GET", """ Pagina consulta de atividades""", _prefix + """consulta_atividades""")
)
                      

// @LINE:34
def eventos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.eventos(), HandlerDef(this, "controllers.Admin", "eventos", Seq(), "GET", """ Pagina eventos""", _prefix + """eventos""")
)
                      

// @LINE:43
def deletaAtividades(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.deletaAtividades(id), HandlerDef(this, "controllers.Admin", "deletaAtividades", Seq(classOf[Long]), "GET", """""", _prefix + """atividades/$id<[^/]+>/del""")
)
                      

// @LINE:29
def addAtividade(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.addAtividade(), HandlerDef(this, "controllers.Admin", "addAtividade", Seq(), "POST", """""", _prefix + """addAtividades""")
)
                      

// @LINE:28
def atividades(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.atividades(), HandlerDef(this, "controllers.Admin", "atividades", Seq(), "GET", """ Pagina atividades""", _prefix + """atividades""")
)
                      

// @LINE:35
def addEventos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.addEventos(), HandlerDef(this, "controllers.Admin", "addEventos", Seq(), "POST", """""", _prefix + """addEventos""")
)
                      

// @LINE:39
def deletaEventos(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Admin.deletaEventos(id), HandlerDef(this, "controllers.Admin", "deletaEventos", Seq(classOf[Long]), "GET", """""", _prefix + """eventos/$id<[^/]+>/del""")
)
                      
    
}
                          

// @LINE:46
class ReverseAssets {
    

// @LINE:46
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:24
// @LINE:21
// @LINE:18
// @LINE:15
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:21
def ajuda(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.ajuda(), HandlerDef(this, "controllers.Application", "ajuda", Seq(), "GET", """ Pagina Ajuda""", _prefix + """ajuda""")
)
                      

// @LINE:12
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:18
def historia(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.historia(), HandlerDef(this, "controllers.Application", "historia", Seq(), "GET", """ Pagina Historia""", _prefix + """historia""")
)
                      

// @LINE:6
def home(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.home(), HandlerDef(this, "controllers.Application", "home", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:15
def sobre(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.sobre(), HandlerDef(this, "controllers.Application", "sobre", Seq(), "GET", """ Pagina Sobre                """, _prefix + """sobre""")
)
                      

// @LINE:11
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:7
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """""", _prefix + """index""")
)
                      

// @LINE:10
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq(), "GET", """ Authentication""", _prefix + """login""")
)
                      
    
}
                          
}
        
    