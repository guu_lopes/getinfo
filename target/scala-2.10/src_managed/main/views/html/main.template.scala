
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""

<!DOCTYPE html>
<!-- TEMPLATE PADRAO --- APLICATIVOS BY ANDRE F. M. BATISTA -->
<html>
    <head>
    	<!-- define o titulo da pagina -->
        <title>"""),_display_(Seq[Any](/*8.17*/title)),format.raw/*8.22*/(""" BEM-VINDO ! </title>
      
 <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*10.47*/routes/*10.53*/.Assets.at("stylesheets/main.css"))),format.raw/*10.87*/("""">
 <link href=""""),_display_(Seq[Any](/*11.15*/routes/*11.21*/.Assets.at("bootstrap/css/bootstrap.min.css"))),format.raw/*11.66*/("""" rel="stylesheet" media="screen">
 <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*12.52*/routes/*12.58*/.Assets.at("images/favicon.png"))),format.raw/*12.90*/("""">
 <script src=""""),_display_(Seq[Any](/*13.16*/routes/*13.22*/.Assets.at("javascripts/jquery-1.9.1.min.js"))),format.raw/*13.67*/("""" type="text/javascript"></script>
   </head>
   
   <body>
    
    <!-- Para exibicao de conteudo flash 
     	 Por padrão, existem duas areas: success e error
     	 Ambas contam com um link capaz de fazer estas div sumirem
    
    --> 
    
    
    """),_display_(Seq[Any](/*25.6*/if(flash.containsKey("success"))/*25.38*/{_display_(Seq[Any](format.raw/*25.39*/("""
		<div class="alert alert-success">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*28.4*/flash/*28.9*/.get("success"))),format.raw/*28.24*/("""
		</div>
	""")))})),format.raw/*30.3*/("""
	"""),_display_(Seq[Any](/*31.3*/if(flash.containsKey("error"))/*31.33*/{_display_(Seq[Any](format.raw/*31.34*/("""
		<div class="alert alert-error">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*34.4*/flash/*34.9*/.get("error"))),format.raw/*34.22*/("""
		</div>
	""")))})),format.raw/*36.3*/("""
	<!-- fim conteudp flash -->
	
	
	
	<!-- BARRA TOPO -->
	
	"""),_display_(Seq[Any](/*43.3*/components/*43.13*/.topbar())),format.raw/*43.22*/("""
	
	
			
	</p>
	
	 <!--
	<div class="hero-unit">
		<h1> NOME DA APLICACAO </h1> 	
	</div>
		
	<span class="label label-warning"> Atenção: sistema em fase de testes </span>
	
	</p> -->
	
	<!-- Divisao da Pagina em 2 partes 	 -->

	<div class="container-fluid">
		<div class="row-fluid">	
                 
			"""),_display_(Seq[Any](/*63.5*/if(session.contains("connected"))/*63.38*/ {_display_(Seq[Any](format.raw/*63.40*/("""
					"""),_display_(Seq[Any](/*64.7*/components/*64.17*/.menu())),format.raw/*64.24*/("""
			""")))})),format.raw/*65.5*/("""
			<div class="span12">
				<!-- BODY CONTENT -->
				"""),_display_(Seq[Any](/*68.6*/content)),format.raw/*68.13*/("""
			</div>
		</div>
	</div>

      
<br><br><br><br><br><br><br><br><br><br><br><br><br>
	<!-- RODAPE  -->
		
            <footer>
           <div id="footer">
          
	    <p>&copy; 2015 <span class="badge badge-inverse"> FMU - Faculdade Metropolitanas Unidas </span><span class="badge badge-inverse"></span><span class="badge badge-inverse">Trabalhe Conosco</span> <br> Disciplina de Linguagens e Tecnicas de Programacao.</p>
	    </div>
		    </footer>

</html>
"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.HtmlFormat.Appendable = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 06 21:59:34 BRST 2015
                    SOURCE: C:/Users/Gustavo/Downloads/template/template/app/views/main.scala.html
                    HASH: fc71641fb4b838c487a177d4ec01d58c824531e2
                    MATRIX: 778->1|902->31|1093->187|1119->192|1230->267|1245->273|1301->307|1354->324|1369->330|1436->375|1558->461|1573->467|1627->499|1681->517|1696->523|1763->568|2054->824|2095->856|2134->857|2255->943|2268->948|2305->963|2348->975|2386->978|2425->1008|2464->1009|2583->1093|2596->1098|2631->1111|2674->1123|2770->1184|2789->1194|2820->1203|3164->1512|3206->1545|3246->1547|3288->1554|3307->1564|3336->1571|3372->1576|3463->1632|3492->1639
                    LINES: 26->1|29->1|36->8|36->8|38->10|38->10|38->10|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|53->25|53->25|53->25|56->28|56->28|56->28|58->30|59->31|59->31|59->31|62->34|62->34|62->34|64->36|71->43|71->43|71->43|91->63|91->63|91->63|92->64|92->64|92->64|93->65|96->68|96->68
                    -- GENERATED --
                */
            