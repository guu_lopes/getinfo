
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object topbar extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.2*/("""<!-- BARRA TOPO -->
	
	
	
	<div class="navbar">
		<div class="navbar-inner"> 
			<a class="brand" href=""""),_display_(Seq[Any](/*7.28*/routes/*7.34*/.Application.home())),format.raw/*7.53*/(""""><h1><font color="red">FMU<small><br><font color="red">Faculdades Metropolitanas Unidas</font></small></font color="red"></h1></a>
			
			<!-- SPAN PARA LOGIN -->
			 <span class="navbar-text pull-right">
				"""),_display_(Seq[Any](/*11.6*/if(session.contains("connected"))/*11.39*/ {_display_(Seq[Any](format.raw/*11.41*/("""
			 		Bem vindo """),_display_(Seq[Any](/*12.18*/session/*12.25*/.get("connected"))),format.raw/*12.42*/(""" <a href=""""),_display_(Seq[Any](/*12.53*/routes/*12.59*/.Application.logout)),format.raw/*12.78*/(""""> <span class="label lbl-info"> Sair</span> </a>
				 """)))}/*13.7*/else/*13.12*/{_display_(Seq[Any](format.raw/*13.13*/("""
				 <br><br>
			  		<a  href=""""),_display_(Seq[Any](/*15.19*/routes/*15.25*/.Application.login)),format.raw/*15.43*/(""""> <span class="badge badge-success"><h4>Login</h4> <span>  </a> 
			 	""")))})),format.raw/*16.7*/(""" 

			 </span>
			 <!-- FIM SPAN PARA LOGIN -->
		<ul class="text-center">
			<br>
			<a class="divider-vertical"><span class="badge badge-info"><h1><i><font color="white"><marquee>GetInfo.</marquee></span></font></i></h1></a> 
			 
			 
			
			
		</ul>
	</div>
	</div>		
	</p>
	
			 <!-- FIM BARRA TOPO -->
			 
	"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 06 21:59:34 BRST 2015
                    SOURCE: C:/Users/Gustavo/Downloads/template/template/app/views/components/topbar.scala.html
                    HASH: 2f4f11a56289e5c6e767694c20645491fde3ac3d
                    MATRIX: 867->1|1007->106|1021->112|1061->131|1307->342|1349->375|1389->377|1443->395|1459->402|1498->419|1545->430|1560->436|1601->455|1675->511|1688->516|1727->517|1796->550|1811->556|1851->574|1954->646
                    LINES: 29->1|35->7|35->7|35->7|39->11|39->11|39->11|40->12|40->12|40->12|40->12|40->12|40->12|41->13|41->13|41->13|43->15|43->15|43->15|44->16
                    -- GENERATED --
                */
            