
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object loginform extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Form[Application.Login],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formLogin: Form[Application.Login]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.38*/("""

        
        """),_display_(Seq[Any](/*4.10*/helper/*4.16*/.form(routes.Application.authenticate)/*4.54*/ {_display_(Seq[Any](format.raw/*4.56*/("""
            
            <h1><font color="mediumblue">Login</font></h1>
            
            """),_display_(Seq[Any](/*8.14*/if(formLogin.hasGlobalErrors)/*8.43*/ {_display_(Seq[Any](format.raw/*8.45*/(""" 
                <p class="error">
                    """),_display_(Seq[Any](/*10.22*/formLogin/*10.31*/.globalError.message)),format.raw/*10.51*/("""
                </p>
            """)))})),format.raw/*12.14*/("""
            
            """),_display_(Seq[Any](/*14.14*/if(flash.contains("success"))/*14.43*/ {_display_(Seq[Any](format.raw/*14.45*/("""
                <p class="success">
                    """),_display_(Seq[Any](/*16.22*/flash/*16.27*/.get("success"))),format.raw/*16.42*/("""
                </p>
            """)))})),format.raw/*18.14*/("""
            
            <p>
                <input type="email" name="email" placeholder="Email" value=""""),_display_(Seq[Any](/*21.78*/formLogin("email")/*21.96*/.value)),format.raw/*21.102*/("""">
            </p>
            <p>
                <input type="password" name="password" placeholder="Password">
            </p>
            <p>
               	<input type="submit" value="Login" class="btn btn-success">
   
            </p>
            
        """)))})),format.raw/*31.10*/("""

    """))}
    }
    
    def render(formLogin:Form[Application.Login]): play.api.templates.HtmlFormat.Appendable = apply(formLogin)
    
    def f:((Form[Application.Login]) => play.api.templates.HtmlFormat.Appendable) = (formLogin) => apply(formLogin)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 06 21:59:34 BRST 2015
                    SOURCE: C:/Users/Gustavo/Downloads/template/template/app/views/components/loginform.scala.html
                    HASH: 7cd8ec873ba8eb1ca3f9ccfd25f51d80b7a9260a
                    MATRIX: 806->1|936->37|991->57|1005->63|1051->101|1090->103|1224->202|1261->231|1300->233|1393->290|1411->299|1453->319|1520->354|1583->381|1621->410|1661->412|1755->470|1769->475|1806->490|1873->525|2016->632|2043->650|2072->656|2371->923
                    LINES: 26->1|29->1|32->4|32->4|32->4|32->4|36->8|36->8|36->8|38->10|38->10|38->10|40->12|42->14|42->14|42->14|44->16|44->16|44->16|46->18|49->21|49->21|49->21|59->31
                    -- GENERATED --
                */
            