// @SOURCE:C:/Users/Gustavo/Downloads/template/template/conf/routes
// @HASH:5286a5368e71860e1558a443b8d2c7787bf13c96
// @DATE:Thu Nov 12 08:33:56 BRST 2015


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_home0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:7
private[this] lazy val controllers_Application_index1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("index"))))
        

// @LINE:10
private[this] lazy val controllers_Application_login2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:11
private[this] lazy val controllers_Application_authenticate3 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:12
private[this] lazy val controllers_Application_logout4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:15
private[this] lazy val controllers_Application_sobre5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sobre"))))
        

// @LINE:18
private[this] lazy val controllers_Application_historia6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("historia"))))
        

// @LINE:21
private[this] lazy val controllers_Application_ajuda7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("ajuda"))))
        

// @LINE:24
private[this] lazy val controllers_Application_home8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("home"))))
        

// @LINE:28
private[this] lazy val controllers_Admin_atividades9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("atividades"))))
        

// @LINE:29
private[this] lazy val controllers_Admin_addAtividade10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addAtividades"))))
        

// @LINE:34
private[this] lazy val controllers_Admin_eventos11 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("eventos"))))
        

// @LINE:35
private[this] lazy val controllers_Admin_addEventos12 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addEventos"))))
        

// @LINE:38
private[this] lazy val controllers_Admin_consulta_eventos13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("consulta_eventos"))))
        

// @LINE:39
private[this] lazy val controllers_Admin_deletaEventos14 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("eventos/"),DynamicPart("id", """[^/]+""",true),StaticPart("/del"))))
        

// @LINE:42
private[this] lazy val controllers_Admin_consulta_atividades15 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("consulta_atividades"))))
        

// @LINE:43
private[this] lazy val controllers_Admin_deletaAtividades16 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("atividades/"),DynamicPart("id", """[^/]+""",true),StaticPart("/del"))))
        

// @LINE:46
private[this] lazy val controllers_Assets_at17 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.home()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """index""","""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.authenticate()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sobre""","""controllers.Application.sobre()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """historia""","""controllers.Application.historia()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """ajuda""","""controllers.Application.ajuda()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """home""","""controllers.Application.home()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """atividades""","""controllers.Admin.atividades()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addAtividades""","""controllers.Admin.addAtividade()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """eventos""","""controllers.Admin.eventos()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addEventos""","""controllers.Admin.addEventos()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """consulta_eventos""","""controllers.Admin.consulta_eventos()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """eventos/$id<[^/]+>/del""","""controllers.Admin.deletaEventos(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """consulta_atividades""","""controllers.Admin.consulta_atividades()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """atividades/$id<[^/]+>/del""","""controllers.Admin.deletaAtividades(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_home0(params) => {
   call { 
        invokeHandler(controllers.Application.home(), HandlerDef(this, "controllers.Application", "home", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:7
case controllers_Application_index1(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """""", Routes.prefix + """index"""))
   }
}
        

// @LINE:10
case controllers_Application_login2(params) => {
   call { 
        invokeHandler(controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Nil,"GET", """ Authentication""", Routes.prefix + """login"""))
   }
}
        

// @LINE:11
case controllers_Application_authenticate3(params) => {
   call { 
        invokeHandler(controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:12
case controllers_Application_logout4(params) => {
   call { 
        invokeHandler(controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:15
case controllers_Application_sobre5(params) => {
   call { 
        invokeHandler(controllers.Application.sobre(), HandlerDef(this, "controllers.Application", "sobre", Nil,"GET", """ Pagina Sobre                """, Routes.prefix + """sobre"""))
   }
}
        

// @LINE:18
case controllers_Application_historia6(params) => {
   call { 
        invokeHandler(controllers.Application.historia(), HandlerDef(this, "controllers.Application", "historia", Nil,"GET", """ Pagina Historia""", Routes.prefix + """historia"""))
   }
}
        

// @LINE:21
case controllers_Application_ajuda7(params) => {
   call { 
        invokeHandler(controllers.Application.ajuda(), HandlerDef(this, "controllers.Application", "ajuda", Nil,"GET", """ Pagina Ajuda""", Routes.prefix + """ajuda"""))
   }
}
        

// @LINE:24
case controllers_Application_home8(params) => {
   call { 
        invokeHandler(controllers.Application.home(), HandlerDef(this, "controllers.Application", "home", Nil,"GET", """ Pagina Home""", Routes.prefix + """home"""))
   }
}
        

// @LINE:28
case controllers_Admin_atividades9(params) => {
   call { 
        invokeHandler(controllers.Admin.atividades(), HandlerDef(this, "controllers.Admin", "atividades", Nil,"GET", """ Pagina atividades""", Routes.prefix + """atividades"""))
   }
}
        

// @LINE:29
case controllers_Admin_addAtividade10(params) => {
   call { 
        invokeHandler(controllers.Admin.addAtividade(), HandlerDef(this, "controllers.Admin", "addAtividade", Nil,"POST", """""", Routes.prefix + """addAtividades"""))
   }
}
        

// @LINE:34
case controllers_Admin_eventos11(params) => {
   call { 
        invokeHandler(controllers.Admin.eventos(), HandlerDef(this, "controllers.Admin", "eventos", Nil,"GET", """ Pagina eventos""", Routes.prefix + """eventos"""))
   }
}
        

// @LINE:35
case controllers_Admin_addEventos12(params) => {
   call { 
        invokeHandler(controllers.Admin.addEventos(), HandlerDef(this, "controllers.Admin", "addEventos", Nil,"POST", """""", Routes.prefix + """addEventos"""))
   }
}
        

// @LINE:38
case controllers_Admin_consulta_eventos13(params) => {
   call { 
        invokeHandler(controllers.Admin.consulta_eventos(), HandlerDef(this, "controllers.Admin", "consulta_eventos", Nil,"GET", """ Pagina consulta de eventos""", Routes.prefix + """consulta_eventos"""))
   }
}
        

// @LINE:39
case controllers_Admin_deletaEventos14(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.deletaEventos(id), HandlerDef(this, "controllers.Admin", "deletaEventos", Seq(classOf[Long]),"GET", """""", Routes.prefix + """eventos/$id<[^/]+>/del"""))
   }
}
        

// @LINE:42
case controllers_Admin_consulta_atividades15(params) => {
   call { 
        invokeHandler(controllers.Admin.consulta_atividades(), HandlerDef(this, "controllers.Admin", "consulta_atividades", Nil,"GET", """ Pagina consulta de atividades""", Routes.prefix + """consulta_atividades"""))
   }
}
        

// @LINE:43
case controllers_Admin_deletaAtividades16(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Admin.deletaAtividades(id), HandlerDef(this, "controllers.Admin", "deletaAtividades", Seq(classOf[Long]),"GET", """""", Routes.prefix + """atividades/$id<[^/]+>/del"""))
   }
}
        

// @LINE:46
case controllers_Assets_at17(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     